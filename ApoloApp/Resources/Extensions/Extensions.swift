//
//  Extensions.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import UIKit

extension UIImageView{
    func imageFromServerURL(urlString:String, placeHolderImage:UIImage){
           if self.image == nil{
               self.image = placeHolderImage
           }
           if(urlString == ""){
                self.image = placeHolderImage
                return
           }
           URLSession.shared.dataTask(with: URL(string: urlString)!) {(data,response,error) in
               if error != nil {
                   return
               }
               else{
                   DispatchQueue.main.async {
                       guard let data = data else {return }
                       let image = UIImage(data: data)
                       self.image = image
                   }
               }
           }.resume()
       }
}
