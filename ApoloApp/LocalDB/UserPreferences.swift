//
//  UserPreferences.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/5/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation

class UserPreferences{
    
    func saveFavorite(item:Int){
        let defaults = UserDefaults.standard
        var array = defaults.array(forKey: "ItemsArray")  as? [Int] ?? [Int]()
        array.append(item)
        defaults.set(array, forKey: "ItemsArray")
    }
    
    func getFavorites() -> [Int]{
        let defaults = UserDefaults.standard
        let array = defaults.array(forKey: "ItemsArray")  as? [Int] ?? [Int]()
        return array
    }
    
    func DeleteFavorite(item:Int){
        let defaults = UserDefaults.standard
        var array = defaults.array(forKey: "ItemsArray")  as? [Int] ?? [Int]()
        if array.count > 0{

            array.remove(where:  { (obj) -> Bool in
                       return obj == item
                   })
            defaults.set(array, forKey: "ItemsArray")
        }
    }
}
