//
//  DetailViewModel.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/5/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation

class DetailViewModel{
    
    private var managerConnections = ManagerConnection()
       private(set) var view:DetailView?
       private var router: DetailRouter?
       
       func bind(view: DetailView, router: DetailRouter){
           self.view = view
           self.router = router
           //TOOD: Setear la vista en el router
           self.router?.setSourceView(view)
       }
}
