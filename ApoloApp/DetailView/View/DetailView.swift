//
//  DetailView.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/5/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import UIKit
import RxSwift

class DetailView: UIViewController {

    
    var item:ItemModel?
    private var router = DetailRouter()
    private var viewModel = DetailViewModel()
    private var disposeBag = DisposeBag()
    
    //Controler
    

    

    @IBOutlet weak var lbtitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lbdescripcion: UILabel!
    @IBOutlet weak var btnfavorite: UIButton!
    
    @IBOutlet weak var lbfecha: UILabel!
    @IBOutlet weak var lbnasaid: UILabel!
    @IBAction func btnfavoriteclick(_ sender: Any) {
        UIView.animate(withDuration: 0.6,
        animations: {
            self.btnfavorite.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.6) {
                self.btnfavorite.transform = CGAffineTransform.identity
            }
        })
        itemFavorite()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(view: self, router: router)
        lbtitle.text = item?.title
        image.imageFromServerURL(urlString: item!.image, placeHolderImage: UIImage(named: "defaultimage")!)
        lbnasaid.text = item?.nasaID
        lbfecha.text = item?.dateCreated
        lbdescripcion.text = item?.description
        validaFavorito()
    }
    
    func validaFavorito(){
        let array = UserPreferences().getFavorites()
        if(array.count > 0){
            if array.contains(item!.id){
                btnfavorite.setImage(UIImage(named: "favorite"), for: .normal)
            }
            else{
                btnfavorite.setImage(UIImage(named: "nofavorite"), for: .normal)
            }
        }
    }
    
    func itemFavorite()
    {
        var array = UserPreferences().getFavorites()
        if(array.count > 0){
            if array.contains(item!.id){
                btnfavorite.setImage(UIImage(named: "favorite"), for: .normal)
            }
            else{
                UserPreferences().saveFavorite(item: item!.id)
                btnfavorite.setImage(UIImage(named: "favorite"), for: .normal)
            }
        }
        else{
            UserPreferences().saveFavorite(item: item!.id)
            btnfavorite.setImage(UIImage(named: "favorite"), for: .normal)
        }
    }

}
