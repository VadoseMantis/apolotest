//
//  DetailRouter.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/5/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation
import UIKit

class DetailRouter{
    
    var viewController: UIViewController{
         return createViewController()
     }
    
    var item:ItemModel?
    
    private var sourceView:UIViewController?
     
    
    init(item:ItemModel? = nil){
        self.item = item
    }
    
    
    private func createViewController() -> UIViewController{
        let view = DetailView(nibName:"DetailView",bundle: Bundle.main)
        view.item = self.item
        return view
    }
     
     func setSourceView(_ sourceView:UIViewController?){
         guard let view = sourceView else {fatalError("Error desconocido")}
         
         self.sourceView = view
     }
}
