//
//  ManagerConnection.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation
import RxSwift

class ManagerConnection{
    
    func getServerData()-> Observable<[ItemModel]>{
        
        return Observable.create { observer in
            let session = URLSession.shared
            //Normaliza el espacio en la url
            var urlString = Constants.URL.main.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            var request = URLRequest(url : URL(string: urlString!)!)
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request){(data,response,error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else {return}
                
                if response.statusCode == 200{
                    do {
                        
                        let decoder = JSONDecoder()
                        let informationserver = try decoder.decode(HomeModel.self, from: data)
                        var items = [ItemModel]()
                        var id:Int = 0
                        for item in informationserver.collection.items {
                            let newitem : ItemModel?
                            id = id + 1
                            if(item.links == nil){
                                newitem = ItemModel(pId:id, phref: item.href, pmedia: item.data[0].mediaType, ptitle: item.data[0].title,pimage: "",pnasaID: item.data[0].nasaID,pdateCreated: item.data[0].dateCreated,pdescription: item.data[0].datumDescription)
                            }
                            else{
                                newitem = ItemModel(pId:id,phref: item.href, pmedia: item.data[0].mediaType, ptitle: item.data[0].title,pimage: item.links![0].href,pnasaID: item.data[0].nasaID,pdateCreated: item.data[0].dateCreated,pdescription: item.data[0].datumDescription)
                            }
                            items.append(newitem!)
                        }
                        observer.onNext(items)
                        
                    } catch let error  {
                        observer.onError(error)
                        print("Ha ocurrido un error:  \(error.localizedDescription)")
                    }
                }
                else if response.statusCode == 401{
                    print("Error 401")
                }
                else{
                    
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create{
                session.finishTasksAndInvalidate()
            }
        }
    }
}
