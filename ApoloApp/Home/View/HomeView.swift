//
//  HomeView.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class HomeView: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderActivity: UIActivityIndicatorView!
    
    @IBOutlet weak var btndelete: UIButton!
    private var router = HomeRouter()
    private var viewModel = HomeViewModel()
    private var disposeBag = DisposeBag()
    
    private var infoserver = [ItemModel]()
    private var infoserverfiltered = [ItemModel]()
    private var favorites = [ItemModel]()
    
    
   
    //Barra de busqueda
    lazy var searchController: UISearchController = ({
         let controller = UISearchController(searchResultsController: nil)
         controller.hidesNavigationBarDuringPresentation = true
         controller.obscuresBackgroundDuringPresentation = false
         controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .default
         controller.searchBar.backgroundColor = .clear
         controller.searchBar.placeholder = "Buscar un titulo"
         return controller
     })()
    
    //Metodo de inicio de la vista
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Apolo App"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reload", style: .done, target: self, action: #selector(action))

        viewModel.bind(view: self, router: router)
        getDataServer()
        configurateTableView()
        manageSearchBarController()
        
        segmentedControl.addTarget(self, action: #selector(HomeView.indexChanged(_:)), for: .valueChanged)
    }
    
    ///Recarga la informacion desde el servidor
    @objc func action(sender: UIBarButtonItem) {
        loaderActivity.startAnimating()
        loaderActivity.isHidden=false
        getDataServer()
    }
    
    ///Metodo click de eliminar
    @IBAction func btndeleteclick(_ sender: Any) {
        UIView.animate(withDuration: 0.6,
        animations: {
            self.btndelete.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.6) {
                self.btndelete.transform = CGAffineTransform.identity
            }
        })
        infoserver.removeAll()
        showAlertNoItems()
        reloadTableView()
    }
    
    
    @objc
    func indexChanged(_ sender: UISegmentedControl) {
        //Todo
        if segmentedControl.selectedSegmentIndex == 0 {
            reloadTableView()
        }
        //Favoritos
        else{
            loadFavorites()
        }
    }
    
    private func loadFavorites(){
        let array = UserPreferences().getFavorites()
        favorites = [ItemModel]()
        if array.count > 0{
            for item in infoserver{
                for id in  array{
                    if item.id == id{
                        favorites.append(item)
                        break
                    }
                }
            }
            if favorites.count > 0{
                reloadTableView()
            }
            else{
                showAlert()
            }
        }
        else{
             showAlert()
        }
    }
    
  private func manageSearchBarController(){
        let searchBar = searchController.searchBar
        searchController.delegate = self
        tableView.tableHeaderView = searchBar
        tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        
    searchBar.rx.text
    .orEmpty
    .distinctUntilChanged()
        .subscribe(onNext: { (result) in
            self.infoserverfiltered = self.infoserver.filter({movie in
                self.reloadTableView()
                return movie.title.contains(result)
            })
        }).disposed(by: disposeBag)
    }
    
    func showAlert(){
        segmentedControl.selectedSegmentIndex = 0
        let alert = UIAlertController(title: "Mensaje", message: "No tiene favoritos", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showAlertNoItems(){
        segmentedControl.selectedSegmentIndex = 0
        let alert = UIAlertController(title: "Mensaje", message: "No tiene información para mostrar", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func configurateTableView(){
        tableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
        tableView.estimatedRowHeight = 400.0
        tableView.rowHeight = UITableView.automaticDimension
       
    }
    ///Se comunica con el ViewModel y trae la informcion del servidor
    private func getDataServer(){
        return viewModel.getServerInfo()
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in
                self.infoserver = data
                self.reloadTableView()
            }, onError: { error in
                print("Error \(error)")
            }, onCompleted: {
                
            }).disposed(by: disposeBag)
    }
    
    private func reloadTableView(){
        DispatchQueue.main.async {
            self.loaderActivity.stopAnimating()
            self.loaderActivity.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    ///Elimina  un item del array del server y de los favoritos en caso de existir
    func deleteItem(item : ItemModel){
        infoserver.removeAll(where:  { (obj) -> Bool in
            return obj.id == item.id
        })
        UserPreferences().DeleteFavorite(item: item.id)
        reloadTableView()
    }
}

extension HomeView: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if infoserver.count  <= 0 {
            return 0
        }
        
        if searchController.isActive &&  searchController.searchBar.text != "" && segmentedControl.selectedSegmentIndex == 0{
           return self.infoserverfiltered.count
        }
        else if searchController.isActive &&  searchController.searchBar.text != "" && segmentedControl.selectedSegmentIndex == 1{
           return self.infoserverfiltered.count
        }
        else if segmentedControl.selectedSegmentIndex == 1 && favorites.count > 0{
            return self.favorites.count
        }
        else{
            return  self.infoserver.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as! CustomCell
        if infoserver.count  <= 0 {
            return cell
        }
        
        if searchController.isActive &&  searchController.searchBar.text != "" && segmentedControl.selectedSegmentIndex == 0
        {
           cell.itemTitle.text = infoserverfiltered[indexPath.row].title
            if(infoserverfiltered[indexPath.row].image != ""){
                
                cell.imageView?.imageFromServerURL(urlString: infoserverfiltered[indexPath.row].image, placeHolderImage: UIImage(named: "defaultimage")!)
                              cell.imageView?.clipsToBounds = true

            }
        }
        else if searchController.isActive &&  searchController.searchBar.text != "" && segmentedControl.selectedSegmentIndex == 1
        {
           cell.itemTitle.text = infoserverfiltered[indexPath.row].title
            if(infoserverfiltered[indexPath.row].image != ""){
                
                cell.imageView?.imageFromServerURL(urlString: infoserverfiltered[indexPath.row].image, placeHolderImage: UIImage(named: "defaultimage")!)
                cell.imageView?.clipsToBounds = true

            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 && favorites.count > 0{
            cell.itemTitle.text = favorites[indexPath.row].title
            if(favorites[indexPath.row].image != ""){
                
                cell.imageView?.imageFromServerURL(urlString: favorites[indexPath.row].image, placeHolderImage: UIImage(named: "defaultimage")!)
                cell.imageView?.clipsToBounds = true

            }
        }
        else
        {
            cell.itemTitle.text = infoserver[indexPath.row].title
            if(infoserver[indexPath.row].image != ""){
                cell.imageView?.imageFromServerURL(urlString: infoserver[indexPath.row].image, placeHolderImage: UIImage(named: "defaultimage")!)
                cell.imageView?.clipsToBounds = true

            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           if searchController.isActive &&  searchController.searchBar.text != "" && segmentedControl.selectedSegmentIndex == 0 {
            viewModel.makeDetailView(item: infoserverfiltered[indexPath.row])
           }
          else if segmentedControl.selectedSegmentIndex == 1 && favorites.count > 0{
                viewModel.makeDetailView(item: favorites[indexPath.row])
            }
           else{
            viewModel.makeDetailView(item: infoserver[indexPath.row])
           }
       }
    
    
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
    
        
        if(segmentedControl.selectedSegmentIndex == 0){
            let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
                UserPreferences().saveFavorite(item: self.infoserver[editActionsForRowAt.row].id)
            }
            favorite.backgroundColor = .green

            let share = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                self.deleteItem(item: self.infoserver[editActionsForRowAt.row])
            }
            share.backgroundColor = .red

            return [share, favorite]
        }
        else{
            let share = UITableViewRowAction(style: .normal, title: "Delete favorite") { action, index in
                UserPreferences().DeleteFavorite(item: self.favorites[editActionsForRowAt.row].id)
                self.loadFavorites()
            }
            share.backgroundColor = .red

            return [share]
        }
    }
}

extension HomeView: UISearchControllerDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchController.isActive = false
        reloadTableView()
    }
}


extension Array where Element: Equatable {

    @discardableResult mutating func remove(object: Element) -> Bool {
        if let index = index(of: object) {
            self.remove(at: index)
            return true
        }
        return false
    }

    @discardableResult mutating func remove(where predicate: (Array.Iterator.Element) -> Bool) -> Bool {
        if let index = self.index(where: { (element) -> Bool in
            return predicate(element)
        }) {
            self.remove(at: index)
            return true
        }
        return false
    }

}
