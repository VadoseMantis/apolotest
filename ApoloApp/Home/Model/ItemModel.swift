//
//  ItemModel.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/5/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation

struct ItemModel{
    
    let id:Int
    let href:String
    let media_type:String
    let title:String
    let image:String
    let nasaID:String
    let dateCreated:String
    let description:String
    
    init(pId:Int,phref:String,pmedia:String,ptitle:String,pimage:String,pnasaID:String,
         pdateCreated:String,pdescription:String ) {
        self.href = phref
        self.media_type = pmedia
        self.title = ptitle
        self.image =  pimage
        self.nasaID =  pnasaID
        self.dateCreated =  pdateCreated
        self.description =  pdescription
        self.id = pId
    }
}
