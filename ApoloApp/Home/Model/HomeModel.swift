//
//  HomeModel.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct HomeModel: Codable {
    let collection: Collection

}

// MARK: - Collection
struct Collection: Codable {
    let href: String
    let items: [Item]
    let metadata: Metadata
    let links: [CollectionLink]?
    let version: String
}

// MARK: - Item
struct Item: Codable {
    let href: String
    let data: [Datum]
    let links: [ItemLink]?
}

// MARK: - Datum
struct Datum: Codable {
    let dateCreated: String
    let nasaID, title, datumDescription: String
    let keywords: [String]?
    let mediaType: String
    let center: String
    let location: String?
    let photographer: String?
    let album: [String]?

    enum CodingKeys: String, CodingKey {
        case dateCreated = "date_created"
        case nasaID = "nasa_id"
        case title
        case datumDescription = "description"
        case keywords
        case mediaType = "media_type"
        case center, photographer, album
        case location = "location"
    }
}




// MARK: - ItemLink
struct ItemLink: Codable {
    let href: String
    let rel: Rel
    let render: String?
}

enum Rel: String, Codable {
    case captions = "captions"
    case preview = "preview"
}

// MARK: - CollectionLink
struct CollectionLink: Codable {
    let href: String
    let rel, prompt: String
}

// MARK: - Metadata
struct Metadata: Codable {
    let totalHits: Int

    enum CodingKeys: String, CodingKey {
        case totalHits = "total_hits"
    }
}
