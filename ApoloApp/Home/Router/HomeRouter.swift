//
//  HomeRouter.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation
import UIKit


//Clase que permite retornar las vistas de Home
class HomeRouter{
    
    var viewController: UIViewController{
           return createViewController()
    }
    
    private var sourceView :UIViewController?
    
    ///Retorna la vista HomeView.XIB
    private func createViewController() -> UIViewController{
        let view = HomeView(nibName: "HomeView", bundle: Bundle.main)
        return view
    }
    
    func setSourceView(_ sourceView:UIViewController?){
        guard let view = sourceView else {fatalError("Error desconocido")}
        self.sourceView = view
    }
    
    func navigateToDetailView(item:ItemModel){
       let detailView = DetailRouter(item: item).viewController
       sourceView?.navigationController?.pushViewController(detailView, animated: true)
    }
}
