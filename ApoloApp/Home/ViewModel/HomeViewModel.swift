//
//  HomeViewModel.swift
//  ApoloApp
//
//  Created by Oscar Julian on 7/4/20.
//  Copyright © 2020 Oscar Julian. All rights reserved.
//

import Foundation
import RxSwift

class HomeViewModel{
    
    //Propiedad de la vista
    private weak var view:HomeView?
    //Ruteador
    private var router:HomeRouter?
    
    //Instancia a la capa de conexion
    private var managerConnection = ManagerConnection()
    
    //Metodo de enlace con la vista y con el router
    func bind(view: HomeView,router:HomeRouter){
        self.view = view
        self.router = router
        self.router?.setSourceView(view)
    }
    
    
    //Funcion que obtiene la informacion del servidor
    //Para pasarla a la vista
    func getServerInfo() -> Observable<[ItemModel]>{
        return managerConnection.getServerData()
    }
    
    func makeDetailView(item:ItemModel){
           router?.navigateToDetailView(item: item)
       }
}
